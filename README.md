### Postmarket OS Extra Devel

![banner](/artwork/src/banner.png)

unofficial external repo meant as testing ground for extra PmOS artwork. mainly its for wallpaper but i do love making other artworks too. fell free to give suggestion and feedback.

folder structure
1. artwork - non wallpaper works and draft pages.
1. current - current development meant for next PmOS release / rsync folder.
1. draft - occasionally i made multiple wallpaper and select only the best for current
1. release - prior released and accepted PmOS extra wallpaper archive.

special permission granted for official PmOS dev to reuse and relicense my artworks as needed for PostmarketOS demand.
